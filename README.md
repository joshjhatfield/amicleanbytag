# amicleanbytag

Cleans AMI's based on tags, by default dryrun is enabled and is set to 30 days.


For help:
```
amicleanbytag.py --help
```


Use:
```
python amicleanbytag.py --tag=tag1 --value=key1 --days=45 --dryrun=True
```


Options:

  --tag TEXT      The aws tag associated, REQUIRED

  --value TEXT    The key value for your tag, REQUIRED

  --days INTEGER  Amis older than x days, default is <30>

  --dryrun TEXT   <True> or <False> accepted, default is True

  --help          Show this message and exit.