import os
import sys
import click
import datetime
import boto3

MyDir = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, MyDir+"/deps")

os.chdir(MyDir)


stsacct= boto3.client('sts')
ec2 = boto3.client('ec2')








# Get aws account ID
def GetacctId():
    account_id = stsacct.get_caller_identity()["Account"]
    return account_id



# Parse out and delte the image required
@click.command()
@click.option('--tag', help='The aws tag associated, REQUIRED')
@click.option('--value', help='The key value for your tag, REQUIRED')
@click.option('--days', default=30, help='Amis older than x days, default is <30>')
@click.option('--dryrun', default='True', help='<True> or <False> accepted, default is True')
def GetAMIList(tag, value, days, dryrun):
    Images = ec2.describe_images(
        Filters = [
            {
                'Name': 'tag-key',
                'Values': [tag]
            },
            {
                'Name': 'tag-value',
                'Values': [value]
            }
        ],
        Owners=[GetacctId()]
    )
    for ImageDate in Images['Images']:
        CreatedDateTime = datetime.datetime.strptime(ImageDate['CreationDate'][:10], '%Y-%m-%d')
        LastDayToKeep = datetime.datetime.now() - datetime.timedelta(days=days)

        if CreatedDateTime < LastDayToKeep and dryrun == 'False':
            try:
                ec2.deregister_image(
                    ImageId=ImageDate['ImageId'],
                    DryRun=False
                )
                print ("%s %s %s %s" % ("Deregistered", ImageDate['ImageId'], "from", CreatedDateTime))
            except:
                print ("%s %s %s %s" % ("ERROR: Could not deregister", ImageDate['ImageId'], "from", CreatedDateTime))

        elif CreatedDateTime < LastDayToKeep and dryrun == 'True':
            print ("%s %s %s" % ('Dryrun:', ImageDate['ImageId'], CreatedDateTime))


if __name__ == '__main__':
    GetAMIList()




